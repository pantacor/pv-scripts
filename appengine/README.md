# Pantavisor App Engine

Welcome to Pantavisor App Engine!

To start using it, you can just use our [self-contained App Engine delivery package](https://docs.pantahub.com/requirements-appengine/).

However, if you need to install it in a different target, which can be useful for prototyping, you can use the installer. To get it, you have to [build Pantavisor](https://docs.pantahub.com/environment-setup/) from your host:

```
repo init -u https://gitlab.com/pantacor/pv-manifest -m release.xml -g runtime,x64-appengine
repo sync -j10 -c --no-clone-bundle
PANTAVISOR_DEBUG=yes ./build.docker.sh x64-appengine
```

The installer tarball can be found in `out/x64-appengine/x64-appengine.installer.tar.xz`.

To install it, just uncompress the tarball and execute the script in the target device:

```
tar -xf /tmp/appengine/appengine.tar.xz -C /tmp/appengine/
sudo /tmp/appengine/install /opt/pantavisor
```

The installation script should inform you about how to run it:

```
sudo /opt/pantavisor/pantavisor -i
```

Once you are finished, you can uninstall it using this script:

```
sudo /opt/pantavisor/uninstall
```
