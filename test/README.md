# Pantavisor Test Framework

The Pantavisor Test Framework contains a set of scripts to help with automated testing on Pantavisor taking advantage of [Pantavisor App Engine](https://docs.pantahub.com/requirements-appengine/).

## Requirements

Before starting, you will need to get App Engine by following our [how to guide](https://docs.pantahub.com/requirements-appengine/).

## How to Run All Tests

To run all tests:

```shell
./test.docker.sh run
```

This might take a while, but you can always stop it with CTRL-C.

## How to Run a Test

Tests can be individually run too.

First, it is important to know which tests are available:

```shell
./test.docker.sh ls
```

For example, to run the test number 0 from the 'local' group of tests. This will require sudo privileges:

```shell
./test.docker.sh run local:0
```

To run all tests from the local group of tests:

```shell
./test.docker.sh run local
```

To run the tests with the network simulator, just add the -n option:

```shell
./test.docker.sh run -n local:1
```

The network simulator will setup the following network interfaces both in the App Engine container and in the host:

* wlan0: A virtual wireless interface that offers an access point with SSID _PVWiFi_ and pass _SuperHardPass_.

## How to Create a New Test

To create a new test inside of the 'local' test group:

```shell
./test.docker.sh add local
```

This will create the necessary templates for a new test in `tests/local/data/roles`. These templates can then be edited manually to get the desired test.

## How to Modify the behavior of an Existing Test

If we go to any newly created test using the previous command, we will find a directory and a JSON file:

```shell
output  resources  test.json
```

The test.json file contains the necessary metadata information for the execution and evaluation of a test:

```json
{
        "#spec": "pv-test@1",
        "description": "ls query to mgmt and non-mgmt containers",
		"setup": {
                "cmdline": "",
                "env": "",
                "pantavisor.config": "../../common/configs/pantavisor.config",
                "pvs": "../../common/pvs/*.tar.gz",
                "containers": {
                        "control": "pvr-sdk",
                        "tarballs": [
                        ],
                        "urls": [
                                "https://pvr.pantahub.com/pantahub-ci/pv_tests_assets/3#pvr-sdk/",
                                "https://pvr.pantahub.com/pantahub-ci/pv_tests_assets/3#pvr-sdk_norole/"
                        ]
                },
                "self-claim": "false",
				"ready-script": "resources/ready",
        },
        "test-script": "resources/test",
        "skip": "false"
}
```

### test.json

#### `#spec`

Version of the test.json. Only supported value is 'pv-test@1'.

#### `description`

Brief description of the test objective.

#### `setup`

Initial Pantavisor setup for the test.

##### `cmdline`

Substitutes what is read from /proc/cmdline. Can be used for [configuraiton](https://docs.pantahub.com/pantavisor-configuration) purposes.

##### `env`

Emulates Linux environment variables by adding the variables as Docker ones. Can be used for [configuraiton](https://docs.pantahub.com/pantavisor-configuration) purposes.

##### `pantavisor.config`

Pantavisor [configuration](https://docs.pantahub.com/pantavisor-configuration/#pantavisorconfig) file that will be used for the test.

##### `pvs`

Tarball with the [secureboot](https://docs.pantahub.com/storage/#state-signature) certs and keys. This makes possible to use tests from different manifests in our own.

##### `containers`

Initial container setup for the test.

###### `control`

The name of the container that will be used by the Test Framework utilities as the [management](https://docs.pantahub.com/containers/#roles) controller for things such as initialization and shutdown.

It is important to set a control container for the correct operation of the test (unless what is being tested is a device without management containers), and it is important to notice that the container must be provided either in the list of [tarballs](#tarballs) or the list of [urls](#urls).

###### `tarballs`

List of [tarballs](https://docs.pantahub.com/pvr/#pvr-export) to be installed in the first [revision](https://docs.pantahub.com/revisions/) for the test. Can be left empty.

###### `urls`

List of [Clone URLs](https://docs.pantahub.com/ph-device-dashboard/) to be installed in the first [revision](https://docs.pantahub.com/revisions/) for the test. Can be left empty.

##### `ready-script`

POSIX-compliant sh script to be executed before evaluating if Pantavisor status is READY. Can be used to force a READY status for any container.

##### `self-claim`

Pantavisor device will be [claimed in PantacorHub](https://docs.pantahub.com/clone-your-system/) and later deleted when the test exits. It requires `PH_USER` and `PH_PASS` env variables to be defined with [PantacorHub credentials](https://docs.pantahub.com/register-user/).

#### `test-script`

The test itself. Must be a POSIX-compliant sh script. The stdout and stderr of it will form the [result](#how-to-modify-the-result-of-a-test) of the test.

#### `skip`

Whether to skip a test or not. Possible values are 'true' or 'false'.

The test will still be executable [individually](#how-to-run-a-test) or [interactively](#how-to-debug-a-test).

## How to Debug a Test

To interactively run a test:

```shell
./test.docker.sh run local:0 -i
```

A console will be opened with an instance of App Engine already running with the parameters that were set in [test.json](#testjson), except the [test script](#test-script) will not be automatically run.

If Pantavisor is not getting to the READY status, you can use the manual option plus the interactive one:

```shell
./test.docker.sh run local:0 -i -m
```

With this, the console will be opened right before starting Pantavisor. You will be able to start it with a modified configuration, such as enabling [stdoud logs](https://docs.pantahub.com/storage/#standard-output) for debugging.

## How to Modify the Result of a Test

To create or overwrite the results of a test:

```shell
./test.docker.sh run local:0 -o
```
