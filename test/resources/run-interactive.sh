#!/bin/sh

echo "Welcome to Pantavisor appengine!"
echo ""
echo "If you run Pantavisor in the background, remember to leniently shut it down using lxc-attach with your control container in the --name option."
echo ""
echo "/opt/pantavisor/pantavisor                               Run Pantavisor"
echo "/opt/pantavisor/pantavisor &                             Run Pantavisor in the backgroup"
echo "lxc-attach --name=pvr-sdk -- pvcontrol commands poweroff Leniently shutdown Pantavisor"
echo ""

sh -c "bash --rcfile /opt/pantavisor/set_env"
