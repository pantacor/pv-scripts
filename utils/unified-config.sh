#!/bin/sh

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <prefix> <input_file>"
  exit 1
fi

prefix="$1"
input_file="$2"
temp_file="$(mktemp)"

while IFS='=' read -r key value; do
  # Replace "." with "_" and convert to uppercase, add the provided prefix
  new_key="${prefix}$(echo "$key" | tr '[:lower:]' '[:upper:]' | tr '.' '_')"
  echo "$new_key=$value" >> "$temp_file"
done < "$input_file"

# Move the temporary file to overwrite the original file
mv "$temp_file" "$input_file"
