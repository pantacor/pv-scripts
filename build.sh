#!/bin/bash

set -ex

export PANTAVISOR_VERSION="${PANTAVISOR_VERSION:-NA}"
export PANTAVISOR_DEBUG=${PANTAVISOR_DEBUG:-yes}
export PVR_DISABLE_SELF_UPGRADE=true
export XZ_DEFAULTS='-T0'

if [ -z "$1" ]; then
	echo "Must define target product as first argument [arm-qemu, malta-qemu, arm-generic, mips-generic, x64-generic, arm-mvebu, arm-rpi3, arm-rpi2, arm-rpi4, x64-uefi]"
	exit 1
fi

REPO_SYNC_MSG="Try syncing the repo using: .repo/repo/repo sync -j10 -c --no-clone-bundle"

TARGET=$1
export TARGET

TOP_DIR=$(cd $(dirname $0) && pwd -P)

cpus=`cat /proc/cpuinfo | grep processor | wc -l`
threads=$(($cpus + 1))

if test -z "$MAKEFLAGS"; then
	MAKEFLAGS="-j$threads"
fi

export MAKEFLAGS
mkdir -p out/

if [ -n "$PVR_SIG_DISABLE" ]; then
	:
elif [ -n "$PVR_X5C_PATH" ]; then
	PVR_X5C_PATH=`realpath $PVR_X5C_PATH`
elif [ -n "$PVR_X5C_PATH_BASE64" ]; then
	tmpf=`mktemp -t x5c.crt.XXXXXXXXXX`
	echo $PVR_X5C_PATH_BASE64 | base64 -d > $tmpf
	PVR_X5C_PATH=$tmpf
	tmpf=
elif [ -f ca/pvs/pvs.defaultkeys.tar.gz ]; then
	tar -C out -xvf ca/pvs/pvs.defaultkeys.tar.gz
	PVR_X5C_PATH=`realpath out/pvs/x5c.default.pem`
fi

if [ -f ca/pvs/pvs.oemkeys.tar.gz ]; then
	tar -C out -xvf ca/pvs/pvs.oemkeys.tar.gz
fi

export PVR_X5C_PATH

if [ -n "$PVR_SIG_DISABLE" ]; then
	:
elif [ -n "$PVR_SIG_KEY" ]; then
	PVR_SIG_KEY=`realpath $PVR_SIG_KEY`
elif [ -n "$PVR_SIG_KEY_BASE64" ]; then
	tmpf=`mktemp -t sig.key.XXXXXXXXXX`
	echo $PVR_SIG_KEY_BASE64 | base64 -d > $tmpf
	PVR_SIG_KEY=$tmpf
elif [ -f ca/pvs/pvs.defaultkeys.tar.gz ]; then
	tar -C out -xvf ca/pvs/pvs.defaultkeys.tar.gz
	PVR_SIG_KEY=`realpath out/pvs/key.default.pem`
fi

export PVR_SIG_KEY

PV_FIT_SIG_DIR=`mktemp -d -t pvr_fit_sig_dir.XXXXXXXXX`
PV_FIT_SIG_KEY=$PVBUILD_FIT_SIG_KEY
PV_FIT_SIG_KEY_BASE64=$PVBUILD_FIT_SIG_KEY_BASE64
if [ -n "$PV_FIT_SIG_KEY" ]; then
	cp -v $PV_FIT_SIG_KEY $PV_FIT_SIG_DIR/pvfitsig.key
elif [ -n "$PV_FIT_SIG_KEY_BASE64" ]; then
	tmpf=`mktemp -t sig.key.XXXXXXXXXX`
	echo $PV_FIT_SIG_KEY_BASE64 | base64 -d > $PV_FIT_SIG_DIR/pvfitsig.key
fi

PVBUILD_TARGET=
PVBUILD_UBOOT=
bootfs=

prepare_uboot_env_files() {
	boot_scr=${TOP_DIR}/config/${PVBUILD_TARGET}/boot.scr
	uboot_env=${TOP_DIR}/config/${PVBUILD_TARGET}/uboot.env
	if [ "${PANTAVISOR_DEBUG}" == "no" ]; then
		if [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/boot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.scr ]; then
			boot_scr=${TOP_DIR}/config/${PVBUILD_TARGET}/boot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.scr
		fi
		if [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/uboot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.env ]; then
			uboot_env=${TOP_DIR}/config/${PVBUILD_TARGET}/uboot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.env
		fi
	else
		if [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/boot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.debug.scr ]; then
			boot_scr=${TOP_DIR}/config/${PVBUILD_TARGET}/boot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.debug.scr
		elif [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/boot.debug.scr ]; then
			boot_scr=${TOP_DIR}/config/${PVBUILD_TARGET}/boot.debug.scr
		elif [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/boot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.scr ]; then
			boot_scr=${TOP_DIR}/config/${PVBUILD_TARGET}/boot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.scr
		fi

		if [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/uboot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.debug.env ]; then
			uboot_env=${TOP_DIR}/config/${PVBUILD_TARGET}/uboot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.debug.env
		elif [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/uboot.debug.env ]; then
			uboot_env=${TOP_DIR}/config/${PVBUILD_TARGET}/uboot.debug.env
		elif [ -e ${TOP_DIR}/config/${PVBUILD_TARGET}/uboot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.env ]; then
			uboot_env=${TOP_DIR}/config/${PVBUILD_TARGET}/uboot${PVBUILD_SUBTARGET:+.${PVBUILD_SUBTARGET}}.env
		fi
	fi
}

restore_kernel_logo() {
	if [ -f ${KERNEL_LOGO}.backup ]; then
		mv ${KERNEL_LOGO}.backup ${KERNEL_LOGO} -f
	fi
}

setup_kernel() {
	setup_kernel_atom

	KERNEL_LOGO_DIR=
	if [ -d ${kerneldir}/drivers/video/logo/ ]; then
		KERNEL_LOGO_DIR=${kerneldir}/drivers/video/logo/
	fi

	VENDOR_KERNEL_LOGO=
	logo_file_name=
	# inject logo
	if [ -d "${SUBTARGET_VENDOR_DIR}" ]; then
		logos=${SUBTARGET_VENDOR_DIR}/config/*.ppm
		if [ ${logos[0]} != ${SUBTARGET_VENDOR_DIR}/config/\*.ppm ]; then
			VENDOR_KERNEL_LOGO=${logos[0]}
		fi
	fi

	if [ -z "${VENDOR_KERNEL_LOGO}" ] && [ -d "${TARGET_VENDOR_DIR}" ]; then
		logos=${TARGET_VENDOR_DIR}/config/*.ppm
		if [ ${logos[0]} != ${TARGET_VENDOR_DIR}/config/\*.ppm ]; then
			VENDOR_KERNEL_LOGO=${logos[0]}
		fi
	fi

	if [ -z "${VENDOR_KERNEL_LOGO}" ]; then
		echo "custom kernel splash not found"
		return 0
	fi

	logo_file_name=$(basename ${VENDOR_KERNEL_LOGO})

	KERNEL_LOGO=${kerneldir}/drivers/video/logo/
	if [ -f ${kerneldir}/drivers/video/logo/${logo_file_name} ]; then
		KERNEL_LOGO=${kerneldir}/drivers/video/logo/${logo_file_name}
	else
		echo "Error: There is no kernel splash ${logo_file_name} to get replaced by /vendor/config/${logo_file_name}"
		exit 1
	fi

	restore_kernel_logo || true

	mv ${KERNEL_LOGO} ${KERNEL_LOGO}.backup
	cp ${VENDOR_KERNEL_LOGO} ${KERNEL_LOGO} -f
}

fixup_vendor_skel() {
	chmod -R 0644 vendor/*/skel/etc/pantavisor/ssh/* || true
}

setup_kernel_atom() {
	find "$TOP_DIR/kernel/" -not -path "$TOP_DIR/kernel/modules/*" -iname "atom.mk" | xargs rm -f
	if [ -n "${PVBUILD_KERNEL}" ]; then
	    kerneldir=$TOP_DIR/kernel/$PVBUILD_KERNEL.$PVBUILD_SUBTARGET
	    if ! [ -d "$kerneldir" ]; then
		kerneldir=$TOP_DIR/kernel/$PVBUILD_KERNEL
	    fi
	    if [ ! -f "$kerneldir/atom.mk" ]; then
		echo "Setting up $kerneldir kernel"
		ln -s "$TOP_DIR/scripts/atoms/${PVBUILD_KERNEL_ATOM:-kernel-mk}" "$kerneldir/atom.mk"
	    fi
	fi
}

setup_uboot() {
	find "$TOP_DIR/bootloader/" -iname "atom.mk" | xargs rm -f
	if [ -n "${PVBUILD_UBOOT}" ]; then
	    ubootdir=$TOP_DIR/bootloader/$PVBUILD_UBOOT.$PVBUILD_SUBTARGET
	    if ! [ -d $ubootdir ]; then
		ubootdir=$TOP_DIR/bootloader/$PVBUILD_UBOOT
	    fi
	    if [ ! -z "$PVBUILD_UBOOT" ]; then
		if [ ! -f "$ubootdir/atom.mk" ]; then
			echo "Setting up $ubootdir bootloader"
			ln -s "$TOP_DIR/scripts/atoms/uboot-mk" "$ubootdir/atom.mk"
		fi
	    fi
	fi
}

setup_alchemy () {
	export ALCHEMY_HOME=${TOP_DIR}/alchemy
	export ALCHEMY_WORKSPACE_DIR=${TOP_DIR}

	export ALCHEMY_TARGET_PRODUCT=traild
	export ALCHEMY_TARGET_PRODUCT_VARIANT=$PVBUILD_TARGET
	export ALCHEMY_TARGET_CONFIG_DIR=${TOP_DIR}/config/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	echo TARGET=$ALCHEMY_TARGET_PRODUCT_VARIANT
	export ALCHEMY_TARGET_OUT=${TOP_DIR}/out/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	if [ -n "$SUBTARGET" ]; then
		ALCHEMY_TARGET_OUT=$ALCHEMY_TARGET_OUT-$SUBTARGET
		export SUBTARGET_VENDOR_DIR=${TOP_DIR}/vendor/${ALCHEMY_TARGET_PRODUCT_VARIANT}-$SUBTARGET
	fi
	export ALCHEMY_USE_COLORS=1

	export TRAIL_BASE_DIR=${ALCHEMY_TARGET_OUT}/trail/
	export TRAIL_FINAL_DIR=${ALCHEMY_TARGET_OUT}/trail/final/
	export TARGET_VENDOR_DIR=${TOP_DIR}/vendor/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	export PVR="${TOP_DIR}/scripts/pvr/pvr"
	if [ "${PANTAVISOR_DEBUG}" == no ]; then
		export KCPPFLAGS='-DENV_DEVICE_SETTINGS_EXTRA=\"silent=1\\0\"'
	fi
	if [ -z "$(ls -A ${ALCHEMY_TARGET_CONFIG_DIR})" ]; then
		echo "Directory config/${ALCHEMY_TARGET_PRODUCT_VARIANT} doesn't exists or is empty! ${REPO_SYNC_MSG}" 1>&2
		exit 2
	fi
	export MCOPY=${ALCHEMY_TARGET_OUT}/staging-host/usr/bin/mcopy

	if [ -e config/${PVBUILD_TARGET}/${PVBUILD_IMAGE_TYPE}.image.config ]; then
	    set +a
	    source config/${PVBUILD_TARGET}/${PVBUILD_IMAGE_TYPE}.image.config
	    set -a
	fi

	export PVBUILD_MMC_SIZE="${MMC_SIZE:-$PVBUILD_MMC_SIZE}"
	export PVBUILD_BOOT_SIZE="${BOOT_SIZE:-$PVBUILD_BOOT_SIZE}"
	export PVBUILD_BOOTAB_SIZE="${BOOT_SIZE:-$PVBUILD_BOOTAB_SIZE}"
	export PVBUILD_MMC_OTHER_MKPART="${MMC_OTHER_MKPART:-$PVBUILD_MMC_OTHER_MKPART}"
	export PVBUILD_MMC_OTHER_PART_SIZES="${MMC_MMC_OTHER_PART_SIZES:-$PVBUILD_MMC_OTHER_PART_SIZES}"
	export PVBUILD_KERNEL_ATOM="${KERNEL_ATOM:-$PVBUILD_KERNEL_ATOM}"
	if ! [ "${PVBUILD_KERNEL_ATOM/-rpi-/}" = "${PVBUILD_KERNEL_ATOM}" ]; then
		PVBUILD_MULTI_KERNEL=yes
	fi
	export PVBUILD_MULTI_KERNEL
}

setup_trail() {
    if [ "$PVBUILD_DM_CONVERT" = "yes" ]; then
	PVR_DM_CONVERT=yes
    else
	PVR_DM_CONVERT=
    fi
    export PVR_DM_CONVERT
}

build_mmc_tools() {
	${ALCHEMY_HOME}/scripts/alchemake host.e2fsprogs
	${ALCHEMY_HOME}/scripts/alchemake host.mtools
}

create_oem_config_partition() {

	local in_file="$1"
	# OEM config
	_config_bin_dir=${TOP_DIR}/out/${PVBUILD_TARGET}/binary_config
	_config_bin_file=${_config_bin_dir}/compaq.bin
	mkdir -p $_config_bin_dir
	rm -f $_config_bin_file

	_root_label=pvroot
	_root_src_dir=${ALCHEMY_TARGET_OUT}/trail/final/
	_delete_dirs=
	if echo $TARGET | grep -q -- -installer; then
		_root_label=pvdata
		_orig_src_dir=$_root_src_dir
		_root_src_dir=`mktemp -d -t pvinstall-data.XXXXXX`
		_delete_dirs="$delete_dirs $_root_src_dir"

		mkdir -p $_root_src_dir/.pvf/

		# install files in vendor pvfactory dir
		cp -rf ${TOP_DIR}/vendor/${PVBUILD_TARGET}/pvfactory/* $_root_src_dir/.pvf

		# local pvfactory env; use for vendor settings/credentials etc.
		if [ -n "$PV_FACTORY_LOCAL" -a -d "$PV_FACTORY_LOCAL" ]; then
			cp -rfv ${PV_FACTORY_LOCAL}/* $_orig_src_dir/.pvf/
		fi

		# XXX TODO
		tmpf=$(mktemp)
		sha256sum -b ${TOP_DIR}/vendor/${PVBUILD_TARGET}/boot/grub/grub.cfg | awk '{ print $1 }' > $tmpf

		cp -f $tmpf $_root_src_dir/.pvf/config/grub.cfg.sha256
		cp -f ${TOP_DIR}/out/${PVBUILD_TARGET}/trail/final/trails/0/.pv/pv-kernel.img $_root_src_dir/.pvf/pv-kernel.img

		if [ "$SDO_INSTALLER" = "yes" ]; then
			cp -f ${TOP_DIR}/vendor/${PVBUILD_TARGET}/boot-installer/pv-installer-sdo.img $_root_src_dir/.pvf/pv-installer.img
		else
			cp -f ${TOP_DIR}/vendor/${PVBUILD_TARGET}/boot-installer/pv-installer.img $_root_src_dir/.pvf/pv-installer.img
		fi

		# pack up root
		fakeroot tar -C $_orig_src_dir -cvJf $_root_src_dir/.pvf/root.tar.xz .

		if [ -n "$PV_AUTOTOK_FILE" ]; then
			mkdir -p $_root_src_dir/config/
			if [ ! -f "$PV_AUTOTOK_FILE" ]; then
				PANTAHUB_HOST=${PANTAHUB_HOST:-api.pantahub.com}
				PANTAHUB_PORT=${PANTAHUB_PORT:-443}
				PH_BASE_URL="https://$PANTAHUB_HOST:$PANTAHUB_PORT"
				echo "Lets create a new device token to be used by the pvf-installer"
				echo "Enter your pantahub username:"
				read username
				echo "Enter your password: (password won't be show)"
				read -s password

				# Get user token to be used to create the device_token
				response=$(curl --request POST ${PH_BASE_URL}/auth/login \
					--header 'content-type: application/json' \
					--data '{"username": "'${username}'","password": "'${password}'"}')

				token=`echo $response | jq -r '.token'`
				error=`echo $response | jq -r '.Error'`

				if [ "$token" == "null" ] || [ "$error" != "null" ]; then
					echo "server error: $error"
					echo ""
					echo "Your aren't authorized, review the username and password you used here."
					echo ""

					exit 1
				fi

				# Create a new device token
				response=`curl --request POST ${PH_BASE_URL}/devices/tokens --header "Authorization: Bearer $token"`

				id=`echo $response | jq -r '.id'`
				error=`echo $response | jq -r '.Error'`
				if [ "$id" == "null" ] || [ "$error" != "null" ]; then
					echo "server error: $error"
					echo ""
					echo "We can't get a device token correctly, try to get it and put it in $PV_AUTOTOK_FILE"
					echo "In order to get the device token you could use"
					echo "curl --request POST ${PH_BASE_URL}/devices/tokens --header 'Authorization: Bearer $token'"
					echo ""
					exit 1
				fi
				echo $response > $PV_AUTOTOK_FILE
			fi

			echo -n -e "autojointoken.aca.json=$(cat $PV_AUTOTOK_FILE)\0" >> $_config_bin_file
		fi

		# pack up boot
		cp -f $bootfs $_root_src_dir/.pvf/boot.bin.$PVBUILD_BOOT_SIZE
	fi
	rm -f $bootfs

	# read oem configargs.txt from vendor
	configargs=
	if [ -f ${TARGET_VENDOR_DIR}/oem/configargs.txt ]; then
		configargs=`cat ${TARGET_VENDOR_DIR}/oem/configargs.txt`
	fi

	# inject auto tok into OEM configuration partition (XXX: fix for grub)
	if [ -n "$PV_FACTORY_AUTOTOK" ]; then
		echo -n -e "configargs=${configargs}${configargs:+ }ph_factory.autotok=${PV_FACTORY_AUTOTOK}\0" >> $_config_bin_file
	elif [ -n "$PV_OEM_CONFIGARGS" ]; then
		echo -n -e "configargs=$PV_OEM_CONFIGARGS\0" >> $_config_bin_file
	elif [ -n "$configargs" ]; then
		echo -n -e "configargs=$configargs\0" >> $_config_bin_file
	fi

	# add 1M control partition for OEM configuration
	if [ -n "$in_file" ]; then
		echo -n -e "\0" >> $_config_bin_file
		echo "creating control partition"
		parted -s $in_file -- \
			mkpart p fat32 ${size_i}MiB $(($size_i + 1))MiB
		if [ $partno = 0 ]; then
			( echo t; echo 17; echo w; echo q ) | fdisk $in_file
		else
			( echo t; echo $(($partno + 1)); echo 17; echo w; echo q ) | fdisk $in_file
		fi
		offset=`sfdisk -J $in_file | jq -r .partitiontable.partitions[$partno].start`
		size=`sfdisk -J $in_file | jq -r .partitiontable.partitions[$partno].size`
		if [ -f $_config_bin_file ]; then
			dd conv=notrunc if=$_config_bin_file of=$in_file bs=512 seek=$offset count=$size
		else
			dd conv=notrunc if=/dev/zero of=$in_file bs=512 seek=$offset count=$size
		fi
		size_i=$(($size_i + 1))
		partno=$(($partno + 1))
		sync $in_file
	fi

}

# Writes bootloader binaries to temporary block image boot patition using dd command
# Also serves as a place to provide custom hook to write vendor/user specific firmware binaries
# to boot partition of temporary block image file
# Input: The temporary block image file created via mktemp in block) case statement
# Output: Same temporary block image file written with boot firmware binaries using dd command
write_raw_bootloader_files() {

	local out_file="$1"

	if [ -n "$out_file" ]; then
		if [ -n "${PVBUILD_BOOTLOADER_RAW_MANGLE_HOOK}" ]; then
			if [ -x "${ALCHEMY_TARGET_CONFIG_DIR}"/"${PVBUILD_BOOTLOADER_RAW_MANGLE_HOOK}" ] || \
				[ -n "${PVBUILD_BOOTLOADER_RAW_MANGLE_INPATH}" ]; then
				PVBUILD_BOOTLOADER_RAW_MANGLE_OUTFILE="$out_file"
				${ALCHEMY_TARGET_CONFIG_DIR}/${PVBUILD_BOOTLOADER_RAW_MANGLE_HOOK} \
					${TOP_DIR}/$PVBUILD_BOOTLOADER_RAW_MANGLE_INPATH $PVBUILD_BOOTLOADER_RAW_MANGLE_OUTFILE
			else
				echo "Bootloader Raw Mangle Hook script either doesn't exists or is not executable or Raw mangle Input path is not defined!"
				exit 2
			fi
		fi
		if [ -n "$PVBUILD_BOOTLOADER_RAW_INFILE" ]; then
			if [ -z "$PVBUILD_BOOTLOADER_RAW_SEEK" ] || [ -z "$PVBUILD_BOOTLOADER_RAW_BS" ]; then
				echo "One more variables for writing raw boot binaries doesn't exists in boot_${bootloader_variant}_build.env."
				exit 1
			fi
			if [ -s "${ALCHEMY_TARGET_OUT}/${PVBUILD_BOOTLOADER_RAW_INFILE}" ]; then
				dd if=${ALCHEMY_TARGET_OUT}/${PVBUILD_BOOTLOADER_RAW_INFILE} of=$out_file \
					bs=${PVBUILD_BOOTLOADER_RAW_BS} seek=${PVBUILD_BOOTLOADER_RAW_SEEK} conv=notrunc
			elif [ -s "${TARGET_VENDOR_DIR}/boot/${PVBUILD_BOOTLOADER_RAW_INFILE}" ]; then
				dd if=${TARGET_VENDOR_DIR}/boot/${PVBUILD_BOOTLOADER_RAW_INFILE} of=$out_file \
					bs=${PVBUILD_BOOTLOADER_RAW_BS} seek=${PVBUILD_BOOTLOADER_RAW_SEEK} conv=notrunc
			else
				echo "${ALCHEMY_TARGET_OUT}/${PVBUILD_BOOTLOADER_RAW_INFILE} doesn't exists!"
				exit 2
			fi
		fi
	else
		echo "The input provided to write_raw_bootloader_files() is empty!"
	fi
}

# create the bootimg fat blob
create_fat_bootimg() {
	local tmpfs=$1

	# delete in case its a overwrite
	rm -f $tmpfs

	echo "making vfat boot fs image with size ${PVBUILD_BOOT_SIZE}MiB"
	dd if=/dev/zero of=${tmpfs} bs=1M count=0 seek=${PVBUILD_BOOT_SIZE}
	mkfs.vfat -n pvboot ${tmpfs} > /dev/null 2>&1
	echo "copying boot contents to \"$tmpfs\""
	if [ -n "${TARGET_VENDOR_DIR}" -a -d "${TARGET_VENDOR_DIR}/boot" ]; then
		"${MCOPY}" -v -i "${tmpfs}" -s "${TARGET_VENDOR_DIR}"/boot/* ::/
	fi
	if [ -n "${SUBTARGET_VENDOR_DIR}" -a -d "${SUBTARGET_VENDOR_DIR}/boot" ]; then
		"${MCOPY}" -v -o -i "${tmpfs}" -s "${SUBTARGET_VENDOR_DIR}"/boot/* ::/
	fi
	if [ -d "${PVBUILD_BOOTLOADER_FAT_IN_PATH}" ]; then
		"${MCOPY}" -i "${tmpfs}" -s "${PVBUILD_BOOTLOADER_FAT_IN_PATH}"/u-boot.bin ::/uboot.bin
	fi
	if [ -e "${boot_scr}" ]; then
		"${MCOPY}" -i "${tmpfs}" -s "${boot_scr}" ::/boot.scr
	elif [ -e "${uboot_env}" ]; then
		"${MCOPY}" -i "${tmpfs}" -s "${uboot_env}" ::/uboot.env
	fi
	echo -e -n 'pv_info_bootimg_version='$RANDOM'\0\0' > tmp.boot.pv_info.env
	"${MCOPY}" -i "${tmpfs}" -s "tmp.boot.pv_info.env" ::/pv-info.env
	rm -f tmp.boot.pv_info.env
}

# Copies bootloader binaries to temporary block image boot patition using MCOPY
# Input: The temporary block image file created via mktemp in block) case statement
# Output: Same temporary block image file with boot firmware binaries copied to it
copy_fat_bootloader_files() {

	local tmpfs=$(mktemp)
	local in_file="$1"

	create_fat_bootimg $tmpfs

	bootfs="$tmpfs"
	sync "$bootfs"

	# if we have a boot-installer directory in vendor/ we will overlay it and use that for the actual
	# installer image ...
	if (echo "${TARGET}" | grep -q -- -installer) && [ -d "${TARGET_VENDOR_DIR}"/boot-installer ]; then
		echo "making installer fat partition 'pvfboot'"
		local tmpfs=$(mktemp)
		cp -vf "$bootfs" "$tmpfs"
		fatlabel "$tmpfs" pvfboot
		"${MCOPY}" -D o -v -i "$tmpfs" -s "${TARGET_VENDOR_DIR}"/boot-installer/grub ::/
		"${MCOPY}" -D o -v -i "$tmpfs" -s "${TARGET_VENDOR_DIR}"/boot-installer/EFI ::/
		sync "$tmpfs"
	fi

	echo "writing boot fs to disk image part 1"
	if [ -n "$in_file" ]; then
		dd conv=notrunc if=$tmpfs of=$in_file bs=1M seek=${PVBUILD_NO_PARTITION_OFFSET}
		sync "$in_file"
		echo "boot fs written to disk image part 1"
	else
		echo "The input provided to copy_fat_bootloader_files() is empty!"
	fi
	rm -f $tmpfs
}

create_mmc_dev() {
	local in_file="$1"
	truncate -s ${PVBUILD_MMC_SIZE}M $in_file
	partno=0
	bootpartno=0
	parted -s $in_file -- \
		mklabel ${PVBUILD_PARTITION_TABLE_TYPE}
	sync ${in_file}
	size_i=${PVBUILD_NO_PARTITION_OFFSET}
}

# Create boot partition for bootloader, bootloader environment and kernel image
# Input: The temporary block image file created via mktemp in block) case statement
# Output: Temporary block image with boot partition
create_boot_partition() {

	local in_file="$1"

	if [ ${PVBUILD_BOOT_SIZE} = 0 ]; then
		return 0
	fi
	if [ -n "$in_file" ]; then
		echo "making boot part"
		parted -s $in_file -- \
			mkpart ${PVBUILD_PARTITION_TYPE} fat32 ${PVBUILD_PARTITION_START}MiB \
				$(($PVBUILD_BOOT_SIZE + ${PVBUILD_PARTITION_START}))MiB

		cp -f $in_file ${TOP_DIR}/out/backup1
		bootpartno=$(( $bootpartno + 1 ))
		partno=$(( $partno + 1))
		size_i=$(( $size_i + $PVBUILD_BOOT_SIZE ))
	else
		echo "The input provided to create_boot_partition() is empty!"
		exit 5
	fi
	if test $? -ne 0; then
		echo "error partitioning image file"
		exit 6
	fi
	sync "$in_file"

}

# Create an ext4 data partition depending on the value of PVBUILD_MMC_OTHER_PART_SIZES
# Input: The temporary block image file created via mktemp in block) case statement
# Output: Temporary block image with ext4 data partition pvol
create_ext4_data_partition() {

	local in_file="$1"

	if [ -n "$in_file" ]; then
		part_i=2
		for part_size in ${PVBUILD_MMC_OTHER_PART_SIZES};
		do
			echo "making ext4 data fs image with size ${part_size}MiB"
			tmpfs=$(mktemp)
			dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$part_size
			fakeroot mkfs.ext4 -L pvol$part_i $tmpfs
			sync $tmpfs
			zerofree $tmpfs
			seek=$(($size_i * 1024))
			echo "writing other part fs to disk image part $part_i with seek=${seek}KiB"
			parted -s $in_file -- ${PVBUILD_MMC_OTHER_MKPART}
			dd conv=notrunc if=$tmpfs of=$in_file bs=1K seek=$seek
			sync $in_file
			size_i=$(($size_i + $part_size))
			part_i=$(($part_i + 1))
			rm -f "$tmpfs"
			partno=$(( $partno + 1))
		done
	else
		echo "The input provided to create_ext4_data_partition() is empty!"
		exit 1
	fi
}

# Create an ext4 storage partition for pantavisor trails data
# Input: The temporary block image file created via mktemp in block) case statement
# Output: Temporary block image with ext4 partition
create_ext4_pv_storage_partition() {
	local in_file="$1"

	if [ -n "$in_file" ]; then
		echo PVBUILD_MMC_SIZE: $PVBUILD_MMC_SIZE
		echo size_i: $size_i
		DATA_SIZE=$(($PVBUILD_MMC_SIZE - $size_i - 1))
		SEEK_K=$(($size_i*1024))
		echo "making ext4 data fs image for pv storage ${DATA_SIZE}MiB"
		local tmpfs=$(mktemp)
		dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$DATA_SIZE
		fakeroot mkfs.ext4 -L $_root_label $tmpfs
		sync $tmpfs
		partno=$(($partno  + 1))
		if [ $bootpartno = 0 ]; then
			bootpartno=$partno
			if [ -e "${boot_scr}" ]; then
				cp -f "${boot_scr}" $_root_src_dir/boot.scr
			elif [ -e "${uboot_env}" ]; then
				cp -f "${uboot_env}" $_root_src_dir/uboot.env
			fi
		fi
		echo "copying trail storage data to \"$tmpfs\""
		fakeroot $POPULATEEXTFS $_root_src_dir $tmpfs
		sync $tmpfs
		zerofree $tmpfs
		echo "writing trail data fs to disk image part 2 with seek in KiB=$SEEK_K"
		parted -s $in_file -- \
			mkpart p ext4 ${size_i}MiB -1MiB
		dd conv=notrunc if=$tmpfs of=$in_file bs=1K seek=${SEEK_K}
		sync $in_file
		echo "trail data fs written to disk image part 2"
		rm -f "$tmpfs"
	else
		echo "The input provided to create_ext4_pv_storage_partition() is empty!"
		exit 1
	fi
}

build_boot_images(){

	export DEBUGFS=${ALCHEMY_TARGET_OUT}/staging-host/usr/sbin/debugfs
	export POPULATEEXTFS=${ALCHEMY_TARGET_OUT}/staging-host/bin/populate-extfs.sh
	MCOPY=${ALCHEMY_TARGET_OUT}/staging-host/usr/bin/mcopy
	if test ! -e $MCOPY; then
		echo cannot find mcopy from mtools to generate vfat partition
		exit 36
	fi
	if test ! -e $POPULATEEXTFS; then
		echo cannot find populate-extfs.sh script to build trail storage
		exit 37
	fi

	if [ -n "$PVBUILD_BOOT_SIZE" -a "$PVBUILD_BOOT_SIZE" -gt 0 ]; then
		create_fat_bootimg ${ALCHEMY_TARGET_OUT}/boot.img.bin
	fi
}

build_mmc_image() {
	export DEBUGFS=/sbin/debugfs
	POPULATEEXTFS=${ALCHEMY_TARGET_OUT}/staging-host/bin/populate-extfs.sh
	MCOPY=/usr/bin/mcopy
	if test ! -e $MCOPY; then
		echo cannot find mcopy from mtools to generate vfat partition
		exit 36
	fi
	if test ! -e $POPULATEEXTFS; then
		echo cannot find populate-extfs.sh script to build trail storage
		exit 37
	fi

	case ${PVBUILD_IMAGE_TYPE} in

		manual)
			echo "building ${PVBUILD_IMAGE_TYPE} image artifacts, seeding skeleton structure to ${ALCHEMY_TARGET_OUT} ..."

			prepare_uboot_env_files

			if [ -n ${PVBUILD_IMAGE_MANUAL_SCRIPT_HOOK} ]; then
				if [ -x ${ALCHEMY_TARGET_CONFIG_DIR}/${PVBUILD_IMAGE_MANUAL_SCRIPT_HOOK} ]; then
					boot_scr=${boot_scr} subtarget=${SUBTARGET} TOP_DIR=${TOP_DIR} PVBUILD_TARGET=${PVBUILD_TARGET} \
					TARGET=${TARGET} ${ALCHEMY_TARGET_CONFIG_DIR}/${PVBUILD_IMAGE_MANUAL_SCRIPT_HOOK} ${ALCHEMY_TARGET_OUT}
				else
					echo "Manual Image creation script doesn't exists or is not executable!"
					echo "Please set it in ${TARGET_DIR} build env file and grant executable permission."
				fi
			else
				echo "PVBUILD_IMAGE_MANUAL_SCRIPT_HOOK is empty in ${PVBUILD_IMAGE_TYPE}.image.config"
				exit 2
			fi

		;;

		wks)
			PVBUILD_WKS_BOOTDIR=${ALCHEMY_TARGET_OUT}
			PVBUILD_WKS_ROOTFSDIR=${TRAIL_FINAL_DIR}

			prepare_uboot_env_files
			if [ -n "${boot_scr}" -a -e "${boot_scr}" ]; then
				cp -f ${boot_scr} ${ALCHEMY_TARGET_OUT}/trail/final/boot/boot.scr
			fi

			PATH=$PATH:$TOP_DIR/external/poky/scripts BUILDDIR=$PWD/tmp/ \
				sh -c "cd $TOP_DIR/external/poky/; . ./oe-init-build-env;
				wic create \
					-o $ALCHEMY_TARGET_OUT/wic-images \
					-D \
					-r $PVBUILD_WKS_ROOTFSDIR \
					-b $PVBUILD_WKS_BOOTDIR \
					-k $PVBUILD_WKS_BOOTDIR \
					-n / \
					${TOP_DIR}/config/${PVBUILD_TARGET}/$PVBUILD_WKS_FILE"
			wic_image=`ls $ALCHEMY_TARGET_OUT/wic-images/*.direct | sort | tail -n1`
			cp -L $wic_image $ALCHEMY_TARGET_OUT/${TARGET}-pv.wic.img
		;;

		block)
			if [ -z "$PVBUILD_BOOTLOADER_VARIANTS" ]; then
				echo "PVBUILD_BOOTLOADER_VARIANTS var is not set in build.env file."
				exit 1
			fi
			# check for variables only if block.image.config is sourced
			# we also have a case for PVBUILD_IMAGE_TYPE=none in which image.config will not be present
			if [ -z "$PVBUILD_BOOT_SIZE" ] || \
				[ -z "$PVBUILD_MMC_SIZE" ] || \
				[ -z "$PVBUILD_PARTITION_TABLE_TYPE" ] || \
				[ -z "$PVBUILD_PARTITION_TYPE" ] || \
				[ -z "$PVBUILD_PARTITION_START" ] || \
				[ -z "$PVBUILD_NO_PARTITION_OFFSET" ]; then

				echo "One or more variables in ${PVBUILD_IMAGE_TYPE}.image.config of $TARGET are missing or not set."
				exit 1
			fi
			echo "making base mmc image"
			tmpimg=$(mktemp)

			create_mmc_dev "$tmpimg"

			create_boot_partition "$tmpimg"

			prepare_uboot_env_files

			for bootloader_variant in $(echo "${PVBUILD_BOOTLOADER_VARIANTS}" | sed "s/,/ /g");
			do
				if [ -s "${ALCHEMY_TARGET_CONFIG_DIR}/boot_${bootloader_variant}_build.env" ]; then
					source ${ALCHEMY_TARGET_CONFIG_DIR}/boot_${bootloader_variant}_build.env
				fi

				if [ "${bootloader_variant}" = "raw" ]; then
					write_raw_bootloader_files "$tmpimg"
				elif [ "${bootloader_variant}" = "fat" ]; then
					copy_fat_bootloader_files "$tmpimg"
				fi
			done

			create_ext4_data_partition "$tmpimg"

			create_oem_config_partition "$tmpimg"

			create_ext4_pv_storage_partition "$tmpimg"

			# now mark bootable partition as on
			parted -s "$tmpimg" -- set $bootpartno boot on

			if [ -n "$_delete_dirs" ]; then
				for d in $_delete_dirs; do
					rm -rf $d
				done
			fi
			if test -n "$tmpimg"; then
				mv $tmpimg $ALCHEMY_TARGET_OUT/${TARGET}-pv-${PVBUILD_MMC_SIZE}MiB.img
				echo -e "\nmmc image avaialble at ${ALCHEMY_TARGET_OUT}/${TARGET}-pv-${PVBUILD_MMC_SIZE}MiB.img"
				echo please flash onto ${TARGET} sd card with dd
			fi
		;;

		envpkg|appengine)
			echo "building root tarball, not image. Skipping preparing the boot partition ..."
			if [ -z "$PVBUILD_IMAGE_PACKAGE_ROOTDEST" ]; then
				echo "ERROR: PVBUILD_IMAGE_PACKAGE_ROOTDEST must be configured for image type ${PVBUILD_IMAGE_TYPE}"
				exit 2
			fi

			if [ "${PVBUILD_IMAGE_TYPE}" = "envpkg" ]; then
				cp -rvf ${TARGET_VENDOR_DIR}/envpkg ${ALCHEMY_TARGET_OUT}/
				tarball_name=${TARGET}-envpkg.tar.xz

				prepare_uboot_env_files

				if [ -n "${boot_scr}" -a -e "${boot_scr}" ]; then
					cp -f ${boot_scr} ${ALCHEMY_TARGET_OUT}/trail/final/boot.scr
				fi
			elif [ "${PVBUILD_IMAGE_TYPE}" = "appengine" ]; then
				cp -rvf ${TOP_DIR}/scripts/appengine ${ALCHEMY_TARGET_OUT}/
				cp -vf ${TOP_DIR}/scripts/pvr/pvr.linux.${TARGET}.tar.gz ${ALCHEMY_TARGET_OUT}/appengine/pvr.tar.gz
				tarball_name=${TARGET}.installer.tar.xz
			fi

			mkdir -p `dirname ${ALCHEMY_TARGET_OUT}/${PVBUILD_IMAGE_TYPE}/${PVBUILD_IMAGE_PACKAGE_ROOTDEST}`
			fakeroot tar -C ${ALCHEMY_TARGET_OUT}/trail/final/ -cvJf ${ALCHEMY_TARGET_OUT}/${PVBUILD_IMAGE_TYPE}/${PVBUILD_IMAGE_PACKAGE_ROOTDEST} .
			if [ -n "$_delete_dirs" ]; then
				for d in $_delete_dirs; do
					rm -rf $d
				done
			fi
			if [ -n "${PVBUILD_BSP_EXPORT_PATH}" ]; then
				mkdir -p `dirname ${ALCHEMY_TARGET_OUT}/${PVBUILD_IMAGE_TYPE}/${PVBUILD_BSP_EXPORT_PATH}`
				sh -c "cd ${ALCHEMY_TARGET_OUT}/trail/final/trails/0; $PVR export ${ALCHEMY_TARGET_OUT}/${PVBUILD_IMAGE_TYPE}/${PVBUILD_BSP_EXPORT_PATH}"
			fi
			tar -C ${ALCHEMY_TARGET_OUT}/${PVBUILD_IMAGE_TYPE} -cvJf ${ALCHEMY_TARGET_OUT}/${tarball_name} .
			echo "${PVBUILD_IMAGE_TYPE} tarball available at ${ALCHEMY_TARGET_OUT}/${tarball_name}"
		;;

		none)
			prepare_uboot_env_files

			echo "building root tarball, not image. skipping preparing the boot partition ..."
			if [ -n "${boot_scr}" -a -e "${boot_scr}" ]; then
				cp -f ${boot_scr} ${ALCHEMY_TARGET_OUT}/trail/final/boot.scr
			fi
			fakeroot tar -C ${ALCHEMY_TARGET_OUT}/trail/final/ -cvJf ${ALCHEMY_TARGET_OUT}/${TARGET}-rootfs.tar.xz .
			echo "Root tarball is available at ${ALCHEMY_TARGET_OUT}/${TARGET}-rootfs.tar.xz"
			if [ -n "$_delete_dirs" ]; then
				for d in $_delete_dirs; do
					rm -rf $d
				done
			fi

		;;

		*)
		echo "Un-supported Image Type specified in build.env"
		;;
	esac

	[ $PVR_EXPORT_SKIP ] || sh -c "cd ${ALCHEMY_TARGET_OUT}/trail/final/trails/0; $PVR export ${ALCHEMY_TARGET_OUT}/${TARGET}.pvr.tgz"
}

build_target_bsp() {
	echo BUILDING Target: $PVBUILD_TARGET
	echo BUILDING Uboot: $PVBUILD_UBOOT
	if [ "$PVR_USE_SRC_BSP" != "yes" ]; then
		${ALCHEMY_HOME}/scripts/alchemake all
		${ALCHEMY_HOME}/scripts/alchemake final
		${ALCHEMY_HOME}/scripts/alchemake image
	elif [ -n "$PVBUILD_UBOOT" ]; then
		${ALCHEMY_HOME}/scripts/alchemake uboot
		${ALCHEMY_HOME}/scripts/alchemake final
	fi
}

build_err_msg() {
	f_path=$(realpath --relative-to="${PWD}/.." "${1}")
	echo "${f_path} does not exists! ${REPO_SYNC_MSG}" 1>&2
	exit 1
}

check_for_config_dir() {
	if [[ "$TARGET" == *"arm-"* ]]; then
		echo "WARNING: Please do not use \"arm-\" prefix in Target name, support for \"arm-\" prefix in Target name will go away in future."
	fi

	target=${TARGET}

	# Loop while config directory is not found
	while ! [ -d "${TOP_DIR}/config/${target}" ]; do
		if [[ "${target}" == *"arm-"* ]]; then # check if target name contains "arm-" prefix
			target=${target#arm-}	# remove "arm-" prefix from target name
			TARGET=${TARGET#arm-}	# remove "arm-" prefix from TARGET name
		elif [[ "${target}" == *-* ]]; then # check if $target contains a hyphen
			target=${target%-*} # strip off shortest suffix with hyphen
		else
			build_err_msg "${target}" # error out if $target doesn't contain any more suffix with hyphens
		fi
	done
	if [ -s "${TOP_DIR}/config/${target}/build.env" ]; then
		sub_target=${TARGET#$target-} # store subtarget if present in string to be used by build.env if required e.g. from "arm-toradex-colibri-imx6_7", store only "colibri-imx6_7"
		if [ "$target" = "$sub_target" ]; then # unset $sub_target if it is same as $TARGET
			sub_target=
		fi
		TARGET_DIR=${target} # Set Variable TARGET_DIR to be used further in this script
		source "${TOP_DIR}/config/${target}/build.env"
		export PVBUILD_BOOT_FILES_FROM_OUT
	else
		build_err_msg "${TOP_DIR}/config/${target}/build.env"
	fi

	if [ "$PV_INSTALLER" = "yes" ] && [ "$target" = "x64-uefi" ]; then
		TARGET="x64-uefi-installer"
	elif [ "$PV_INSTALLER" = "yes" ]; then
		echo "PV_INSTALLER only supported by x64-uefi target"
	exit 1
	fi
}

check_for_config_dir

if [ -z "$PVBUILD_TARGET" ] || [ -z "$PVBUILD_BUILD_TARGETS" ] || [ -z "$PVBUILD_IMAGE_TYPE" ]; then
	echo "Please check $TARGET build.env file. One or more required variables in build.env file are missing or not defined."
	exit 1
fi

if [ -f $PV_FIT_SIG_DIR/pvfitsig.key ]; then
	PVBUILD_FIT_SIG_KEY=pvfitsig
	PVBUILD_FIT_SIG_DIR=$PV_FIT_SIG_DIR
elif [ -n "$PVBUILD_FIT_SIG_KEY" ]; then
	PVBUILD_FIT_SIG_DIR=`realpath $PVBUILD_FIT_SIG_DIR`
fi
export PVBUILD_FIT_SIG_KEY
export PVBUILD_FIT_SIG_DIR

SUBTARGET=${PVBUILD_SUBTARGET}
export SUBTARGET
echo "SUBTARGET=$SUBTARGET"

fixup_vendor_skel
setup_alchemy
setup_kernel
setup_uboot
setup_trail

if test -z "$PANTAHUB_HOST"; then
	PANTAHUB_HOST=api.pantahub.com
fi
if test -z "$PANTAHUB_PORT"; then
	PANTAHUB_PORT=443
fi

if [ -n "$PV_PVS_CERT_PEM" ]; then
	PV_PVS_CERT_PEM=`realpath $PV_PVS_CERT_PEM`
elif [ -n "$PV_PVS_CERT_PEM_BASE64" ]; then
	echo $PV_PVS_CERT_PEM_BASE64 | base64 -d > trust.pem
	export PV_PVS_CERT_PEM=$PWD/trust.pem
elif [ -f ca/pvs/pvs.defaultkeys.tar.gz ]; then
	mkdir -p out/
	tar -C out -xvf ca/pvs/pvs.defaultkeys.tar.gz
	PV_PVS_CERT_PEM=`realpath out/pvs/*.crt`
fi
export PV_PVS_CERT_PEM


if [ ! -z "$2" ]; then
	if [ "$2" == "upload" ]; then
		cd $TRAIL_BASE_DIR/staging
		$PVR putobjects -f https://$PANTAHUB_HOST:$PANTAHUB_PORT/objects
		cd $TOP_DIR
	elif [ "$2" == "shell" ]; then
		/bin/bash
	else
		${ALCHEMY_HOME}/scripts/alchemake "${@:2}"
	fi
else

    if [ "$PVBUILD_IMAGE_TYPE" = "manual" ] || [ "$PVBUILD_IMAGE_TYPE" = "block" ] \
	|| [ "$PVBUILD_IMAGE_TYPE" = "envpkg" ] || [ "$PVBUILD_IMAGE_TYPE" = "none" ] \
	|| [ "$PVBUILD_IMAGE_TYPE" = "wks" ] \
	|| [ "$PVBUILD_IMAGE_TYPE" = "appengine" ]; then
	build_mmc_tools
    fi

    build_target_bsp
    for _targets in ${PVBUILD_BUILD_TARGETS}
    do
	echo "Building ${_targets}"
	${ALCHEMY_HOME}/scripts/alchemake ${_targets}
    done
    if [ "$PVBUILD_IMAGE_TYPE" = "manual" ] || [ "$PVBUILD_IMAGE_TYPE" = "block" ] \
	|| [ "$PVBUILD_IMAGE_TYPE" = "envpkg" ] || [ "$PVBUILD_IMAGE_TYPE" = "none" ] \
	|| [ "$PVBUILD_IMAGE_TYPE" = "wks" ] \
	|| [ "$PVBUILD_IMAGE_TYPE" = "appengine" ]; then
	build_boot_images
	build_mmc_image
    fi
fi

restore_kernel_logo
