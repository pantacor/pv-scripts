#!/bin/bash

set -x
set -e

image_name=trailsd:base
realp=`realpath $0`
dir=`dirname $realp`
base=`basename $realp`

user=`id -n -u`

# its important that our user has the exact HOME of the host
# to allow for using docker from host from inside docker
# for cases where the intermediate docker does not preserve that
# same userscape you can provide HOST_HOME for hinting where to
# get the real home from host
getent passwd $user | sed -e 's#:[^:]*:\([^:]*\)$#:'${HOST_HOME:-$HOME}':\1#' > $dir/passwd.snippet
echo $user > $dir/userid

usercontainer_tag=pv-build:pv-dev-`id -u`

builder_extra_args=
if [ -n "$PV_BUILD_PANTAVISOR_BUILDER_PREFIX" ]; then
	builder_extra_args="$builder_extra_args --build-arg PANTAVISOR_BUILDER_PREFIX=$PV_BUILD_PANTAVISOR_BUILDER_PREFIX"
fi
if [ -n "$PV_BUILD_PANTAVISOR_BUILDER_VERSION" ]; then
	builder_extra_args="$builder_extra_args --build-arg PANTAVISOR_BUILDER_VERSION=$PV_BUILD_PANTAVISOR_BUILDER_VERSION"
fi

sh -c "cd $dir; docker build --tag=pantavisor/pv-build -f Dockerfile.build-base $builder_extra_args ."
sh -c "cd $dir; docker build --tag=$usercontainer_tag -f Dockerfile.build ."


pvr_merge_src_abs=
if [ -d "$PVR_MERGE_SRC" ]; then
	pvr_merge_src_abs=`sh -c "cd $PVR_MERGE_SRC; pwd"`
fi

pvr_merge_opts=
if ! [ -z "$pvr_merge_src_abs" ]; then
	pvr_merge_opts=-v$pvr_merge_src_abs:$pvr_merge_src_abs
fi

pvr_bind_mount_repo_mirror=
if [ -n "$REPO_MIRROR" ]; then
	mirror_dir=${REPO_MIRROR%/mirror*}
	mirror_dir=${mirror_dir#*=}
	pvr_bind_mount_repo_mirror=-v${mirror_dir}:${mirror_dir}
fi

# by default we use -it to enter credentials manually; use false for gitlab ci
docker_interactive=
# XXX: PV_BUILD_INTERACIVE is a typo variant used by CI -> remove eventually...
if [ -z "$PV_BUILD_INTERACTIVE" -a -z "$PV_BUILD_INTERACIVE" ] || \
	[ "$PV_BUILD_INTERACTIVE" = true ] || [ "$PV_BUILD_INTERACIVE" = true ]; then
	docker_interactive="-it"
fi

# mount certificates from host if defined
host_cacert_path=
if [ -n "$PV_BUILD_TLS_USE_HOST_CACERTS" ]; then
	if [ "$PV_BUILD_TLS_USE_HOST_CACERTS" = "default" ]; then
		host_cacert_path="/etc/ssl/certs/ca-certificates.crt"
	else
		host_cacert_path="$PV_BUILD_TLS_USE_HOST_CACERTS"
	fi
fi

# form extra mount arguments
extra_mount_arg=
if [ -n "$host_cacert_path" ]; then
	extra_mount_arg="-v$host_cacert_path:$host_cacert_path"
fi

# we discard all variables with whitespaces for now. We need to escape them if we need that in the future
docker run \
	$docker_interactive \
	-e MAKEFLAGS="$MAKEFLAGS" \
	-v$PWD:$PWD \
	-v${HOST_HOME:-$HOME}:${HOST_HOME:-$HOME} \
	-v${TMPDIR:-/tmp}:${TMPDIR:-/tmp} \
	$extra_mount_arg \
	$pvr_bind_mount_repo_mirror \
	$pvr_merge_opts \
	-w$PWD \
	--user `id -u` \
	--env-file=<(env | grep -v ^PATH | grep -v LD_LIBR | grep -v PKG_ | grep -v PYTHON | grep -v TMPDIR | grep -v LC_ | grep -v [[:space:]]) \
	--rm \
	$usercontainer_tag \
	$@

if [ "$1" = "x64-appengine" ] && [ -z "$2" ]; then
	out_appengine_docker="$dir/../out/x64-appengine/appengine-docker"
	rm -rf "$out_appengine_docker"
	out_appengine_docker_docker="$dir/../out/x64-appengine/appengine-docker/docker"
	mkdir -p "$out_appengine_docker_docker"

	cp -r "$dir/../tests" "$out_appengine_docker/tests"
	cp -r "$dir/../scripts" "$out_appengine_docker/scripts"
	if [ -d "$dir/../vendor/x64-appengine/envpkg" ]; then
		mkdir -p "$out_appengine_docker/vendor/x64-appengine"
		cp -r "$dir/../vendor/x64-appengine/envpkg" "$out_appengine_docker/vendor/x64-appengine/envpkg"
	fi
	ln -f -r -s "$out_appengine_docker/scripts/test/test.docker.sh" "$out_appengine_docker/test.docker.sh"

	# build and package the netsim container
	sh -c "cd $dir/test; docker build --no-cache --tag=pantavisor-netsim -f Dockerfile.netsim ."
	out_appengine_netsim="$out_appengine_docker_docker/x64-appengine-netsim.tar"
	docker save pantavisor-netsim > "$out_appengine_netsim"

	# build the appengine container
	cp $dir/test/resources/fallbear-cmd $dir/../out/x64-appengine/fallbear-cmd
	cp $dir/test/resources/run-interactive.sh $dir/../out/x64-appengine/run-interactive.sh
	sh -c "cd $dir/test; docker build --no-cache --tag=pantavisor-appengine -f Dockerfile.appengine ../../out/x64-appengine"
	# build and package the tester container
	sh -c "cd $dir/test; docker build --no-cache --tag=pantavisor-tester -f Dockerfile.tester ."
	out_appengine_tester="$out_appengine_docker_docker/x64-appengine-tester.tar"
	docker save pantavisor-tester > "$out_appengine_tester"

	out_appengine_docker_tarball="$dir/../out/x64-appengine/x64-appengine.docker.tar.gz"
	cd "$out_appengine_docker"
	tar -czvf "$out_appengine_docker_tarball" .
	cd -
	echo "Appengine docker tarball available at $out_appengine_docker_tarball"
fi
